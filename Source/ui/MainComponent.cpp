/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a)
:  audio (a),
    impulseResponseUI(audio.getImpulseResponse()),
    sourceFilePlayerUI(audio.getSourceFile())
{
    setSize (800, 400);
    addAndMakeVisible(impulseResponseUI);
    addAndMakeVisible(roomUI);
    addAndMakeVisible(sourceFilePlayerUI);
    resized();
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    impulseResponseUI.setBoundsRelative(0.55, 0.3, 0.4, 0.1);
    roomUI.setBoundsRelative(0, 0.05, 0.45, 0.9);
    sourceFilePlayerUI.setBoundsRelative(0.55, 0.1, 0.4, 0.15);
    
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

