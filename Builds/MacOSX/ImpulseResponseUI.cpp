//
//  ImpulseResponseUI.cpp
//  JuceBasicAudio
//
//  Created by Steffan Davies on 17/11/2016.
//
//

#include "ImpulseResponseUI.hpp"

ImpulseResponseUI::ImpulseResponseUI(ImpulseResponse& impulseResponse_) : impulseResponse(impulseResponse_)
{
    loadIRButton.setButtonText("Load IR");
    loadIRButton.setColour(TextButton::buttonColourId, Colours::darkred);
    addAndMakeVisible (&loadIRButton);
    loadIRButton.addListener (this);
}

ImpulseResponseUI::~ImpulseResponseUI()
{
    
}

void ImpulseResponseUI::resized()
{
    loadIRButton.setBoundsRelative(0.01, 0.05, 0.3, 0.9);
}

void ImpulseResponseUI::paint (Graphics& g)
{
    g.setColour(Colours::white);
    g.fillAll();
}

void ImpulseResponseUI::buttonClicked (Button* button)
{
    if (button == &loadIRButton)
    {
        //LOAD IR INTO BUFFER
        //Change button colour to green when IR succesfully loaded
        impulseResponse.loadIR();
    }
}