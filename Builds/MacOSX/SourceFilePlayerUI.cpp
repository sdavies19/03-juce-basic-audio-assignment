//
//  SourceFilePlayerUI.cpp
//  JuceBasicAudio
//
//  Created by Steffan Davies on 17/11/2016.
//
//

#include "SourceFilePlayerUI.hpp"

SourceFilePlayerUI::SourceFilePlayerUI (SourceFilePlayer& filePlayer_) : filePlayer (filePlayer_)
{
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    positionSlider.setSliderStyle(Slider::LinearHorizontal);
    positionSlider.setRange(0.0, 1.0);
    positionSlider.addListener(this);
    addAndMakeVisible(positionSlider);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
}

SourceFilePlayerUI::~SourceFilePlayerUI()
{
    delete fileChooser;
}


//Component
void SourceFilePlayerUI::resized()
{
    playButton.setBounds (0, 0, getHeight(), getHeight() / 2);
    fileChooser->setBounds (getHeight(), 0, getWidth()-getHeight(), getHeight() / 2);
    positionSlider.setBoundsRelative(0.25, 0.5, 0.5, 0.25);
    
}

//Button Listener
void SourceFilePlayerUI::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        filePlayer.setPlaying(!filePlayer.isPlaying());
    }
}

void SourceFilePlayerUI::sliderValueChanged (Slider* slider)
{
    
    if (slider == &positionSlider)
    {
        filePlayer.setPosition(positionSlider.getValue());
    }
    
}


//FilenameComponentListener
void SourceFilePlayerUI::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
            filePlayer.loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}