//
//  ImpulseResponse.cpp
//  JuceBasicAudio
//
//  Created by Steffan Davies on 17/11/2016.
//
//

#include "ImpulseResponse.hpp"


ImpulseResponse::ImpulseResponse()
{
    //position to the start of IRSampleBuffer
    bufferPosition = 0;
    //Initialise and clear buffer
    IRSampleBuffer.setSize (1, bufferSize);
    IRSampleBuffer.clear();
}

ImpulseResponse::~ImpulseResponse()
{
    
    
    
}


void ImpulseResponse::loadIR()
{
    
    AudioFormatManager formatManager; formatManager.registerBasicFormats();
    FileChooser chooser ("Please select an Impulse Response", File::getSpecialLocation (File::userHomeDirectory),
                         formatManager.getWildcardForAllFormats());
    if (chooser.browseForFileToOpen()) {
        File file (chooser.getResult());
        AudioFormatReader* reader = formatManager.createReaderFor (file);
        if (reader != 0)
        {
            IRSampleBuffer.setSize (reader->numChannels, reader->lengthInSamples);
            reader->read (&IRSampleBuffer, 0, reader->lengthInSamples, 0, true, false);
            delete reader;
        }
    }
    
}

float ImpulseResponse::processSample (float input)
{
    float IROutput = 0.f;

        //play
        IROutput = *IRSampleBuffer.getWritePointer (0, bufferPosition);

        //increment and cycle the buffer counter
        ++bufferPosition;
        if (bufferPosition == bufferSize)
            bufferPosition = 0;
    
    return IROutput;
}
