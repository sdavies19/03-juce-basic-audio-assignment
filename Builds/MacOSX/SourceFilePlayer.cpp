//
//  SourceFilePlayer.cpp
//  JuceBasicAudio
//
//  Created by Steffan Davies on 17/11/2016.
//
//

#include "SourceFilePlayer.hpp"

SourceFilePlayer::SourceFilePlayer() : thread("SourceFilePlayThread")
{
    thread.startThread();
    currentAudioFileSource = NULL;
}

/**
 Destructor
 */
SourceFilePlayer::~SourceFilePlayer()
{
    audioTransportSource.setSource(0);//unload the current file
    deleteAndZero(currentAudioFileSource);//delete the current file
    
    thread.stopThread(100);
}

/**
 Starts or stops playback of the looper
 */
void SourceFilePlayer::setPlaying (const bool newState)
{
    if(newState == true)
    {
        audioTransportSource.setPosition(0.0);
        audioTransportSource.start();
    }
    else
    {
        audioTransportSource.stop();
    }
}

/**
 Gets the current playback state of the looper
 */
bool SourceFilePlayer::isPlaying () const
{
    return audioTransportSource.isPlaying();
}


/**
 Loads the specified file into the transport source
 */
void SourceFilePlayer::loadFile(const File& newFile)
{
    // unload the previous file source and delete it..
    setPlaying(false);
    audioTransportSource.setSource (0);
    deleteAndZero (currentAudioFileSource);
    
    // create a new file source from the file..
    // get a format manager and set it up with the basic types (wav, ogg and aiff).
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    
    AudioFormatReader* reader = formatManager.createReaderFor (newFile);
    
    if (reader != 0)
    {
        //currentFile = audioFile;
        currentAudioFileSource = new AudioFormatReaderSource (reader, true);
        
        // ..and plug it into our transport source
        audioTransportSource.setSource (currentAudioFileSource,
                                        32768, // tells it to buffer this many samples ahead
                                        &thread,
                                        reader->sampleRate);
    }
}

//AudioSource
void SourceFilePlayer::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    audioTransportSource.prepareToPlay (samplesPerBlockExpected, sampleRate);
}

void SourceFilePlayer::releaseResources()
{
    audioTransportSource.releaseResources();
}

void SourceFilePlayer::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    audioTransportSource.getNextAudioBlock (bufferToFill);
}

void SourceFilePlayer::setPosition(float newPosition)
{
    float length = audioTransportSource.getLengthInSeconds();
    float position = length * newPosition;
    audioTransportSource.setPosition(position);
    
}