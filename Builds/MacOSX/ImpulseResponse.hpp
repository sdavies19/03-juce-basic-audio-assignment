//
//  ImpulseResponse.hpp
//  JuceBasicAudio
//
//  Created by Steffan Davies on 17/11/2016.
//
//

#ifndef ImpulseResponse_hpp
#define ImpulseResponse_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

class ImpulseResponse
{
public:
    ImpulseResponse();
    
    ~ImpulseResponse();
    
    void loadIR();
    
    /**
     Processes the audio sample by sample.
     */
    float processSample (float input);
    
    
private:
    static const int bufferSize = 88200; //constant
    unsigned int bufferPosition;
    AudioSampleBuffer IRSampleBuffer;
    
};





#endif /* ImpulseResponse_hpp */
