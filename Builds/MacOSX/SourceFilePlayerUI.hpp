//
//  SourceFilePlayerUI.hpp
//  JuceBasicAudio
//
//  Created by Steffan Davies on 17/11/2016.
//
//

#ifndef SourceFilePlayerUI_hpp
#define SourceFilePlayerUI_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "SourceFilePlayer.hpp"

/**
 Gui for the SourceFilePlayer class
 */
class SourceFilePlayerUI :   public Component,
    public Button::Listener,
    public Slider::Listener,
    public FilenameComponentListener
{
public:
    /**
     constructor - receives a reference to a FilePlayer object to control
     */
    SourceFilePlayerUI(SourceFilePlayer& filePlayer_);
    
    /**
     Destructor
     */
    ~SourceFilePlayerUI();
    
    //Component
    void resized() override;
    
    //Button Listener
    void buttonClicked (Button* button) override;
    
    //Slider Listener
    void sliderValueChanged (Slider* slider) override;
    
    //FilenameComponentListener
    void filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged) override;
    
private:
    TextButton playButton;
    FilenameComponent* fileChooser;
    
    SourceFilePlayer& filePlayer;
    
    Slider positionSlider;
    
};

#endif /* SourceFilePlayerUI_hpp */
