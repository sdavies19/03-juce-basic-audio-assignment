//
//  ImpulseResponseUI.hpp
//  JuceBasicAudio
//
//  Created by Steffan Davies on 17/11/2016.
//
//

#ifndef ImpulseResponseUI_hpp
#define ImpulseResponseUI_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "ImpulseResponse.hpp"

//NEED BUTTON AND FILE PLAYER: LOAD FILE INTO BUFFER FOR IR
//OPTION TO SET LOCATION

class ImpulseResponseUI : public Component,
                          public Button::Listener
{
public:
    /**
     constructor - receives a reference to an ImpulseResponse object to control
     */
    ImpulseResponseUI(ImpulseResponse& impulseResponse_);
    
    ~ImpulseResponseUI();
    
    void paint (Graphics& g) override;
    
    void buttonClicked (Button* button) override;
    
    void resized() override;
    
private:
    ImpulseResponse& impulseResponse; //reference to an ImpulseResponse object
    TextButton loadIRButton;
    
};





#endif /* ImpulseResponseUI_hpp */
