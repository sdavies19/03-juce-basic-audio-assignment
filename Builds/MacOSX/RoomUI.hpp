//
//  RoomUI.hpp
//  JuceBasicAudio
//
//  Created by Steffan Davies on 17/11/2016.
//
//

#ifndef RoomUI_hpp
#define RoomUI_hpp

#include <stdio.h>

#include "../../JuceLibraryCode/JuceHeader.h"

class RoomUI : public Component
{
public:
    
    RoomUI();
    
    ~RoomUI();
    
    void paint (Graphics& g) override;
    
private:
    
};



#endif /* RoomUI_hpp */
