//
//  SourceFilePlayer.hpp
//  JuceBasicAudio
//
//  Created by Steffan Davies on 17/11/2016.
//
//

#ifndef SourceFilePlayer_hpp
#define SourceFilePlayer_hpp

#include <stdio.h>

#include "../JuceLibraryCode/JuceHeader.h"

//MAYBE MAKE STATIC BUFFER INSTEAD OF STREAM: CHECK FAIRMANS


/**
 SourceFilePlayer class - streams audio from a file to be convolved.
 */
class SourceFilePlayer : public AudioSource
{
public:
    /**
     Constructor
     */
    SourceFilePlayer();
    
    /**
     Destructor
     */
    ~SourceFilePlayer();
    
    /**
     Starts or stops playback of the looper
     */
    void setPlaying (const bool newState);
    
    /**
     Gets the current playback state of the looper
     */
    bool isPlaying () const;
    
    /**
     Loads the specified file into the transport source
     */
    void loadFile(const File& newFile);
    
    void setPosition(float newPosition);
    
    
    //AudioSource
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate);
    void releaseResources();
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill);
    
private:
    AudioFormatReaderSource* currentAudioFileSource;    //reads audio from the file
    AudioTransportSource audioTransportSource;	// this controls the playback of a positionable audio stream, handling the
    // starting/stopping and sample-rate conversion
    TimeSliceThread thread;                 //thread for the transport source
};


#endif /* SourceFilePlayer_hpp */
